const usuarios = require('../models/usuarios')

function validar_admin(req,res,next){
  console.log("paso por validar_admin")
  if(!
    ("indice" in req.body) || (req.body.indice.length === 0) || (typeof(req.body.indice) !== "number") ||
    (req.indice > usuarios.length - 1))
    {
    return res.status(500).json({"mensaje" : "Parametros Incorrectos"});
  }
  else{
    req.admin = usuarios[req.indice].admin
    next()
  }
}

function validar_ingreso_datos(req,res,next){
      //validacion de campos    
      //Validacion de ingreso de datos nombre y apellido
      if(!(/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u).test(req.body.nombre_y_apellido)){
      return res.status(500).json({"mensaje" : "Ingrese nombre y apellido valido"})
    } 
    
    //validacion de email con uso de @, puntos y caracteres
    if (!(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/).test(req.body.correo_electronico)){
     return res.status(500).json({"mensaje" : "Ingrese un email valido"})
    }
    
    usuarios.forEach(element => { // Verifico que el usuario y email ya no se encuentren registrados
      if (req.body.usuario == element.usuario || req.body.email == element.email){
        return res.status(500).json({"mensaje" : "Usuario ya registrado. Utilice Login para entrar a la plataforma"})
      }
    })

    //Validacion de numero de telefono de varios formatos, admites +, (), -
    if(!(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im).test(req.body.telefono)){
      return res.status(500).json({"mensaje" : "Ingrese un numero de telefono valido"})
    }

    //Validacion de ingreso de direccion
    if((req.body.direccion_Envio).length==0){
      return res.status(500).json({"mensaje" : "Ingrese una direccion correcta"})
    } 

    //validacion de ingreso de password
    if((/\s/).test(req.body.password)){
      return res.status(500).json({"mensaje" : "La contraseña no debe tener espacio en blanco"})
    }else{
    if (!(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,15}[^'\s]/).test(req.body.password)){
        return res.status(500).json({"mensaje" : "Ingresa una contraseña correcta, Minimo 8 caracteres, Maximo 15, Al menos una letra mayúscula, Al menos una letra minucula, Al menos un dígito, No espacios en blanco, Al menos 1 caracter especial"})  
    }
  }
next();    
}

function validar_login(req,res,next){
  req.indice = false
  if(req.usuario.length === 0 || req.password.length === 0){
    return res.status(500).json({"mensaje" : "Debe completar todos los campos"});
  }
  for(let element in usuarios){
    if ((req.body.usuario == usuarios[element].usuario &&
       (req.body.password == usuarios[element].password)))
    {
      req.indice = element;
      break
    }
  }
  if (req.indice){
    next()
  }else{
    return res.status(500).json({"mensaje" : "Los datos de usuario y contraseña no coinciden"})
  }
}

module.exports = {validar_admin, validar_login, validar_ingreso_datos}
