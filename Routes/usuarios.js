const express = require('express')
const router = express.Router()
const { validar_admin, validar_login, validar_ingreso_datos} = require("../middlewares/usuarios");


  //Consultar usuarios registrados
  router.get('/', function (req, res) {  // datos de usuario, solo como admin
    if(req.admin){
      res.status(200).json({usuarios})
    }
    else{
      res.send('Restringido')
    }
  })
  
  router.get('/login/', validar_login, function(req,res){
    console.log("get/usuarios/login")
    if(req.indice != false){
      res.status(200).json({"indice" : req.indice})
    }
  })

  router.post('/login/', validar_login, function(req,res){
    if(req.indice != false){
      res.status(200).json({"indice" : req.indice})
    }
  })

  //Crear usuarios nuevos, cualquier usuario puede crear un usuarios.
  router.post('/', validar_ingreso_datos, function (req, res) {
    console.log("get/usuarios/crear")
    usuarios.push(req.body)    
    usuarios[(usuarios.length)-1].admin=false;
    res.send('Usuario Creado')
})
  
  //actualizar usuarios existentes con permiso solo del administrador.
  router.put('/', validar_admin, validar_ingreso_datos, function (req, res) {
    console.log("get/usuarios/Modificar")
    usuarios[req.body.indice].usuario = req.body.usuario;
    usuarios[req.body.indice].nombre_y_apellido = req.body.nombre_y_apellido;
    usuarios[req.body.indice].correo_electronico = req.body.correo_electronico;
    usuarios[req.body.indice].direccion_Envio = req.body.direccion_Envio;
    usuarios[req.body.indice].password = req.body.password;
    usuarios[req.body.indice].admin=req.body.admin;
    res.send('Usuario Actualizado')
  })

  //Eliminar usuarios existentes solo accesos administrador.
  router.delete('/', validar_admin, function (req, res) {  // eliminar un usuario
    console.log("delete /usuarios/")
    usuarios.splice(req.body.indice,1);
    res.send('Usuario Eliminado')  
  })
  
  module.exports = router;